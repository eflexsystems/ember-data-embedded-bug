module.exports = function(app) {
  var express = require('express');
  var childrenRouter = express.Router();

  childrenRouter.get('/', function(req, res) {
    res.send({
      children: [
        { id: 1, captions: [{ text: 'parent', language: 'english' }], children: []}
      ]
    });
  });

  childrenRouter.put('/:id', function(req, res) {
    res.send({
      children: [
        { id: 1, captions: [{ text: 'parent', language: 'english' }], children: [
          { id: 2, parent: 1, captions: [{ text: 'child', language: 'english' }], children: [] }
        ]}
      ]
    });
  });

  childrenRouter.post('/', function(req, res) {
    res.send({
      children: [
        { id: 2, parent: 1, captions: [{ text: 'child', language: 'english' }], children: []}
      ]
    });
  });


  //childrenRouter.get('/:id', function(req, res) {
    //res.send({
      //'children': {
        //id: req.params.id
      //}
    //});
  //});

  //childrenRouter.delete('/:id', function(req, res) {
    //res.status(204).end();
  //});

  app.use('/children', childrenRouter);
};
