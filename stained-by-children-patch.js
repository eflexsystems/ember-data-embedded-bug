save: function() {
  return this
    ._super()
    .then(
      function() {
        this._processChildren(function(child) {
          // changed
          var fragments = child._internalModel._fragments;
          var fragment;
          for (var key in fragments) {
            if (fragment = fragments[key]) {
              fragment._adapterDidCommit();
            }
          }
          //
          child.set('_internalModel._attributes', {});
        });
      }.bind(this)
  );
},
