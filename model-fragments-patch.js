// packages/model-fragments/lib/fragments/model.js
_adapterDidCommit: function(data) {
  var internalModel = model$fragments$lib$fragments$model$$internalModelFor(this);
  internalModel.setupData({
    // changed data || {}
    attributes: data || internalModel._attributes
  });
},
