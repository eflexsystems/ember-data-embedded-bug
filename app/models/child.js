import DS from 'ember-data';
import Ember from 'ember';
import StainedByChildrenMixin from 'stained-by-children/stained-by-children';
import CleanEmbeddedChildrenMixin from 'stained-by-children/clean-embedded-children';

export default DS.Model.extend(StainedByChildrenMixin, CleanEmbeddedChildrenMixin, {
  parent:   DS.belongsTo('child', { inverse: 'children' }),
  children: DS.hasMany('child', { embeddedChild: true, stains: true, inverse: 'parent' }),
  captions: DS.hasManyFragments('caption', { defaultValue: [] }),

  name: Ember.computed('captions.@each.{text,language}', {
    get: function() {
      let caption = this.get('captions').findBy('language', 'english');
      if (caption) {
        return this.get('captions').findBy('language', 'english').get('text');
      }
    },
    set: function(key, value) {
      let caption = this.get('captions').findBy('language', 'english');
      if (caption) {
        caption.set('text', value);
        return caption.get('text');
      }
    }
  })
});
