import DS from 'ember-data';

export default DS.ModelFragment.extend({
  language: DS.attr('string'),
  text: DS.attr('string')
});
