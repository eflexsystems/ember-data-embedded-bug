import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return this.get('store').query('child', {});
  },

  actions: {
    add: function() {
      let store = this.get('store');
      let newChild = store.createRecord('child', {
        id: 2,
        parent: 1,
        captions: [
          store.createFragment('caption', {
            text: 'child',
            language: 'english'
          })
        ]
      });

      this.get('controller.model.firstObject.children').pushObject(newChild);
    },
    save: function(item) {
      item.save();
    },
    rollback: function(item) {
      item.rollbackAttributes();
    }
  }
});
