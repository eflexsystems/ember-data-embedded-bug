import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
    parent: {
      deserialize: false,
      serialize:   'id'
    },
    children: {
      deserialize: 'records',
      serialize:   'records'
    }
  }
});
